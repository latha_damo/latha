﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Log;

namespace Business
{
    public class TitleAccess
    {
        TitleServiceClient client = null;

        /// <summary>
        /// Gets the all possible Titles
        /// </summary>
        /// <param name="SearchText">Input text given by User</param>
        /// <returns>List of possible Title names</returns>
        public List<string> GetTitles(string SearchText)
        {
            List<string> dtTitle = new List<string>();
            try
            {
                client = new TitleServiceClient();
                dtTitle = client.GetTitles(SearchText).ToList();
            }
            catch (Exception ex)
            {
                LogError.WriteLog(ex.Message);
            }
            return dtTitle;
        }

        /// <summary>
        /// Gets the Title Description for the given Title
        /// </summary>
        /// <param name="Title">Title name entered by the User</param>
        /// <returns>Title description</returns>
        public string GetTitleInfo(string Title)
        {
            string result = "";
            try
            {
                client = new TitleServiceClient();
                result = client.GetTitleInfo(Title);
            }
            catch (Exception ex)
            {
                LogError.WriteLog(ex.Message);
            }
            return result;
        }
    }
}
