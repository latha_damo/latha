﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using Log;
using System.Web.Services;

namespace UI
{
    public partial class TitleSearch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnSearch_Click(Object sender, EventArgs e)
        {
            try
            {
                TitleAccess objTA = new TitleAccess();
                if (txtSearch.Value != "")
                {
                    lblTitleInfo.Text = objTA.GetTitleInfo(txtSearch.Value.Trim());
                }
            }
            catch (Exception ex)
            {
                LogError.WriteLog(ex.Message);
            }
        }

        [WebMethod]
        public static List<string> GetAutoCompleteData(string username)
        {
            List<string> result = new List<string>();
            TitleAccess objTA = new TitleAccess();
            result = objTA.GetTitles(username);
            return result;
        }

    }
}