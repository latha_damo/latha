﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TitleSearch.aspx.cs" Inherits="UI.TitleSearch" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AutoComplete Box with jQuery</title>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            SearchText();

        });
        function SearchText() {
            $(".autosuggest").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "TitleSearch.aspx/GetAutoCompleteData",
                        data: "{'username':'" + document.getElementById('txtSearch').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                },
        select: function (event, ui) {
            if (ui.item) {
                
            GetCustomerDetails(ui.item.value);
        }
    },
    minLength: 2
            });
}
function GetCustomerDetails(country) {

    alert(country);

    $.ajax({
        type: "POST",
        url: "TitleSearch.aspx/GetCustomers",
        data: '{country: "' + "country" + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            alert("hi");
            response(data.d);
        },
        failure: function (response) {
            alert("hiError");
            alert(response.d);
        }
    });
}

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblTitleName" runat="server" Text="Title Names: "></asp:Label>
    <asp:DropDownList ID="ddlTitleNames" runat="server" OnSelectedIndexChanged="ddlTitleNames_SelectedIndexChanged"
        AutoPostBack="true" />
    <br />
    <div>
        <asp:Label ID="lblTitleInfo" runat="server" CssClass="txtctrl" Height="207px" Width="569px "></asp:Label>
    </div>
    <div class="demo">
        <div class="ui-widget">
            <label for="tbAuto">
                Enter UserName:
            </label>
            <input type="text" id="txtSearch" class="autosuggest" />
        </div>
    </div>
    </form>
</body>
</html>
