﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Business;
using Log;
using System.Web.Services;

namespace UI
{
    public partial class TitleSearch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!Page.IsPostBack)
            {
                try
                {
                    TitleAccess objTA = new TitleAccess();
                    ddlTitleNames.DataSource = objTA.GetTitles();
                    ddlTitleNames.DataTextField = "TitleName";
                    ddlTitleNames.DataValueField = "TitleID";

                    // Bind the data to the control.
                    ddlTitleNames.DataBind();

                    if (ddlTitleNames.SelectedValue != "")
                    {
                        int intTitleID = Convert.ToInt16(ddlTitleNames.SelectedValue);
                        lblTitleInfo.Text = objTA.GetTitleInfo(intTitleID);
                    }
                }
                catch (Exception ex)
                {
                    LogError.WriteLog(ex.Message);
                }
            }
        }

        protected void ddlTitleNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                TitleAccess objTA = new TitleAccess();
                if (ddlTitleNames.SelectedValue != "")
                {
                    Int32 intTitleID = Convert.ToInt32(ddlTitleNames.SelectedValue);
                    lblTitleInfo.Text = objTA.GetTitleInfo(intTitleID);
                }
            }
            catch (Exception ex)
            {
                LogError.WriteLog(ex.Message);
            }
        }

        [WebMethod]
        public static List<string> GetAutoCompleteData(string username)
        {
            List<string> result = new List<string>();
            result.Add("one1");
            result.Add("one2");
            result.Add("one");
            return result;
        }

        [WebMethod]
        public static List<string> GetCustomers(string username)
        {
            LogError.WriteLog("GetCustomers");
            List<string> result = new List<string>();
            result.Add("one1");
            result.Add("one2");
            result.Add("one");
            return result;
        }
    }
}