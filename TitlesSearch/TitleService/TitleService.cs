﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Log;

namespace TitleServices
{
    public class TitleService : ITitleService
    {
        TitlesEntities objTE = null;
        public TitleService()
        {
            objTE = new TitlesEntities();
        }

        /// <summary>
        /// Gets the all possible Titles
        /// </summary>
        /// <param name="SearchText">Input text given by User</param>
        /// <returns>List of possible Title names</returns>
        public List<string> GetTitles(string SearchText)
        {
            List<string> dtTitle = new List<string>();
            try
            {
                IEnumerable<Title> Title = from titles in objTE.Titles where titles.TitleName.Contains(SearchText) select titles;

                if (Title.Count() > 0)
                {
                    foreach (var t in Title)
                    {
                        dtTitle.Add(t.TitleName);
                    }
                }
            }
            catch (Exception ex)
            {
                LogError.WriteLog(ex.Message);
            }
            return dtTitle;
        }
        
        /// <summary>
        /// Gets the Title Description for the given Title
        /// </summary>
        /// <param name="Title">Title name entered by the User</param>
        /// <returns>Title description</returns>
        public string GetTitleInfo(string Title)
        {
            string result = "";
            try
            {
                IEnumerable<StoryLine> SL = from sl in objTE.StoryLines 
                                            join ts in  objTE.Titles on sl.TitleId equals ts.TitleId
                                            where ts.TitleName == Title
                                            select sl;

                if (SL.Count() > 0)
                {
                    foreach (var sl in SL)
                    {
                        result = sl.Description;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                LogError.WriteLog(ex.Message);
            }
            return result;
        }

    }
}
