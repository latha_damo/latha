﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Data;

namespace TitleServices
{
    [ServiceContract]
    interface ITitleService
    {
        [OperationContract]
        List<string> GetTitles(string SearchText);

        [OperationContract]
        string GetTitleInfo(string Title);

    }
}
