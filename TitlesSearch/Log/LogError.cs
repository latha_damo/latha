﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace Log
{
    /// <summary>
    /// This class can be access from anywhere in the solution for easy access.
    /// </summary>
    public class LogError
    {
        private static string startupPath = System.AppDomain.CurrentDomain.BaseDirectory + @"\WriteText.txt";

        public static void WriteLog(string strError)
        {            
            System.IO.File.AppendAllText(startupPath, strError+ DateTime.Now.ToString() + Environment.NewLine);
        }
    }
}
